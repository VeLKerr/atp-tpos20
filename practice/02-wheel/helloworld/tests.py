from unittest import TestCase
from helloworld.main import get_message


class HelloTextCase(TestCase):
    def test_helloworld(self):
        self.assertEqual(get_message(), 'hello')
