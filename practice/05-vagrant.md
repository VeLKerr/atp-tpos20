1. Установка плагинов: 
```
vagrant plugin install vagrant-hostmanager 
vagrant plugin install vagrant-vbguest 
vagrant plugin install vagrant-scp # https://github.com/invernizzi/vagrant-scp 
```

2. Развертывание VM

 - `vagrant init`
 - далее редактируем Vagrantfile

 ```
 # -*- mode: ruby -*-
# vi: set ft=ruby :

$update_script = <<SCRIPT
apt update
# apt upgrade -y
apt install tmux mc -y
SCRIPT

Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/bionic64"
  config.hostmanager.enabled = false
  config.hostmanager.manage_host = true
  config.hostmanager.ignore_private_ip = false
  config.ssh.forward_agent = true

  config.vm.define :master do |master|
    config.vm.provider "virtualbox" do |vb|
      vb.cpus = "2"
      vb.memory = "1024"
    end
  master.vm.network :private_network, ip: "10.211.55.200"
  master.vm.hostname = "tpos-master"
  master.vm.provision :hostmanager
  master.vm.provision :shell, :inline => $update_script
  master.vm.provision :shell, inline: <<-SHELL
    sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
    sudo service ssh restart
  SHELL
  end
end
```

3. Добавляем ещё 1 машинку:

```
# -*- mode: ruby -*-
# vi: set ft=ruby :

$update_script = <<SCRIPT
apt update
# apt upgrade -y
apt install tmux mc -y
SCRIPT

Vagrant.configure("2") do |config|
  # config.vm.box = "ubuntu/bionic64"
  config.hostmanager.enabled = false
  config.hostmanager.manage_host = true
  config.hostmanager.include_offline = true
  config.hostmanager.ignore_private_ip = false
  config.ssh.forward_agent = true

  config.vm.define :master do |master|
    master.vm.box = "ubuntu/bionic64"
    master.vm.provider "virtualbox" do |vb|
      vb.cpus = "2"
      vb.memory = "1024"
    end
  master.vm.network :private_network, ip: "10.211.55.200"
  master.vm.hostname = "tpos-master"
  master.vm.provision :hostmanager
  master.vm.provision :shell, :inline => $update_script
  master.vm.provision :shell, inline: <<-SHELL
    sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
    sudo service ssh restart
  SHELL
  end

  config.vm.define :node1 do |node1|
    node1.vm.box = "ubuntu/trusty64"
    node1.vm.provider "virtualbox" do |vb|
      vb.cpus = "1"
      vb.memory = "1024"
    end
  node1.vm.network :private_network, ip: "10.211.55.201"
  node1.vm.hostname = "tpos-node1"
  node1.vm.provision :hostmanager
  node1.vm.provision :shell, :inline => $update_script
  node1.vm.provision :shell, inline: <<-SHELL
    sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
    sudo service ssh restart
  SHELL
  end
end
```
